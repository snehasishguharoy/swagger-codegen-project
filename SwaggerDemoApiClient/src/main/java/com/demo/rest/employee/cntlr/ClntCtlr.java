package com.demo.rest.employee.cntlr;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.rest.api.EmployeeResourceApi;
import com.demo.rest.api.HelloResourceApi;
import com.demo.rest.client.invoker.ApiClient;


@RestController
public class ClntCtlr {
	
	private EmployeeResourceApi empApi=empApi();
	
	private HelloResourceApi helloApi=helloResourceApi();
	
	
	
	@GetMapping("/emps")
	public List<com.demo.rest.client.model.Employee> getAllEmps() {
		return empApi.getAllEmpsUsingGET();
	}
	
	@PostMapping("/post")
	public String helloPost(@RequestBody final String hello){
		String str=helloApi.helloPostUsingPOST(hello);
		
		return str;
	}
	
	@Bean
    public EmployeeResourceApi empApi() {
        return new EmployeeResourceApi(apiClient());
    }
     
    @Bean
    public ApiClient apiClient() {
        return new ApiClient();
    }
    
    @Bean
    public HelloResourceApi helloResourceApi(){
    	return new HelloResourceApi(apiClient());
    }
	
	
	

}
