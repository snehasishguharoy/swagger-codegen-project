package com.demo.rest.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggerDemoApiClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwaggerDemoApiClientApplication.class, args);
	}
}
