
# Employee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**empId** | **Integer** | id of the employee |  [optional]
**fname** | **String** | first name of the employee |  [optional]
**lname** | **String** | last name of the employee |  [optional]



