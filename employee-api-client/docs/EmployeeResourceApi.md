# EmployeeResourceApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllEmpsUsingGET**](EmployeeResourceApi.md#getAllEmpsUsingGET) | **GET** /rest/emp/emps | getAllEmps
[**getEmpByIdUsingGET**](EmployeeResourceApi.md#getEmpByIdUsingGET) | **GET** /rest/emp/{empId} | getEmpById


<a name="getAllEmpsUsingGET"></a>
# **getAllEmpsUsingGET**
> List&lt;Employee&gt; getAllEmpsUsingGET()

getAllEmps

### Example
```java
// Import classes:
//import com.demo.rest.client.invoker.ApiException;
//import com.demo.rest.api.EmployeeResourceApi;


EmployeeResourceApi apiInstance = new EmployeeResourceApi();
try {
    List<Employee> result = apiInstance.getAllEmpsUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EmployeeResourceApi#getAllEmpsUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Employee&gt;**](Employee.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getEmpByIdUsingGET"></a>
# **getEmpByIdUsingGET**
> Employee getEmpByIdUsingGET(empId)

getEmpById

### Example
```java
// Import classes:
//import com.demo.rest.client.invoker.ApiException;
//import com.demo.rest.api.EmployeeResourceApi;


EmployeeResourceApi apiInstance = new EmployeeResourceApi();
Integer empId = 56; // Integer | empId
try {
    Employee result = apiInstance.getEmpByIdUsingGET(empId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EmployeeResourceApi#getEmpByIdUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **empId** | **Integer**| empId |

### Return type

[**Employee**](Employee.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

