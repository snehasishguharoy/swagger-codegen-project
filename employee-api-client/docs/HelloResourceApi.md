# HelloResourceApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**helloPostUsingPOST**](HelloResourceApi.md#helloPostUsingPOST) | **POST** /rest/hello/post | Returns Hello World
[**helloPutUsingPUT**](HelloResourceApi.md#helloPutUsingPUT) | **PUT** /rest/hello/put | Returns Hello World
[**helloUsingGET**](HelloResourceApi.md#helloUsingGET) | **GET** /rest/hello | Returns Hello World


<a name="helloPostUsingPOST"></a>
# **helloPostUsingPOST**
> String helloPostUsingPOST(hello)

Returns Hello World

### Example
```java
// Import classes:
//import com.demo.rest.client.invoker.ApiException;
//import com.demo.rest.api.HelloResourceApi;


HelloResourceApi apiInstance = new HelloResourceApi();
String hello = "hello_example"; // String | hello
try {
    String result = apiInstance.helloPostUsingPOST(hello);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HelloResourceApi#helloPostUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hello** | **String**| hello |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="helloPutUsingPUT"></a>
# **helloPutUsingPUT**
> String helloPutUsingPUT(hello)

Returns Hello World

### Example
```java
// Import classes:
//import com.demo.rest.client.invoker.ApiException;
//import com.demo.rest.api.HelloResourceApi;


HelloResourceApi apiInstance = new HelloResourceApi();
String hello = "hello_example"; // String | hello
try {
    String result = apiInstance.helloPutUsingPUT(hello);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HelloResourceApi#helloPutUsingPUT");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hello** | **String**| hello |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="helloUsingGET"></a>
# **helloUsingGET**
> String helloUsingGET()

Returns Hello World

### Example
```java
// Import classes:
//import com.demo.rest.client.invoker.ApiException;
//import com.demo.rest.api.HelloResourceApi;


HelloResourceApi apiInstance = new HelloResourceApi();
try {
    String result = apiInstance.helloUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HelloResourceApi#helloUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

