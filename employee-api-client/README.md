# employee-api-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>com.demo.rest</groupId>
    <artifactId>employee-api-client</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.demo.rest:employee-api-client:0.0.1-SNAPSHOT"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/employee-api-client-0.0.1-SNAPSHOT.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.demo.rest.client.invoker.*;
import com.demo.rest.client.invoker.auth.*;
import com.demo.rest.client.model.*;
import com.demo.rest.api.EmployeeResourceApi;

import java.io.File;
import java.util.*;

public class EmployeeResourceApiExample {

    public static void main(String[] args) {
        
        EmployeeResourceApi apiInstance = new EmployeeResourceApi();
        try {
            List<Employee> result = apiInstance.getAllEmpsUsingGET();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling EmployeeResourceApi#getAllEmpsUsingGET");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://localhost:8080*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*EmployeeResourceApi* | [**getAllEmpsUsingGET**](docs/EmployeeResourceApi.md#getAllEmpsUsingGET) | **GET** /rest/emp/emps | getAllEmps
*EmployeeResourceApi* | [**getEmpByIdUsingGET**](docs/EmployeeResourceApi.md#getEmpByIdUsingGET) | **GET** /rest/emp/{empId} | getEmpById
*HelloResourceApi* | [**helloPostUsingPOST**](docs/HelloResourceApi.md#helloPostUsingPOST) | **POST** /rest/hello/post | Returns Hello World
*HelloResourceApi* | [**helloPutUsingPUT**](docs/HelloResourceApi.md#helloPutUsingPUT) | **PUT** /rest/hello/put | Returns Hello World
*HelloResourceApi* | [**helloUsingGET**](docs/HelloResourceApi.md#helloUsingGET) | **GET** /rest/hello | Returns Hello World


## Documentation for Models

 - [Employee](docs/Employee.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

techprimerschannel@gmail.com

