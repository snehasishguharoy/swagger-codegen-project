package com.demo.rest.client.invoker.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}